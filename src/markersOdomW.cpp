/*-
This code read markers pose, including a fixed frame marker called 0.
markers: 0, 1, 2, 3, 4, 5
version 27  september  0216

Juan José Quiroz Omana




*/

#include <ros/ros.h>
#include <std_msgs/Float64.h>
#include <stdio.h>
#include <time.h>
#include <sys/time.h>

#include <fstream>
#include <cstdlib>
#include <iostream>
#include <string>
#include <cstring>
#include <stdlib.h>
#include <math.h>
#include <dq_robotics/DQ.h>
#include <dq_robotics/DQ_kinematics.h>
#include "geometry_msgs/PoseStamped.h"
#include "geometry_msgs/Pose.h"
#include <tf/transform_broadcaster.h>
#include <nav_msgs/Odometry.h>



#include <Eigen/Dense>

// Markers messages
#include <ar_track_alvar_msgs/AlvarMarkers.h>
//#include <ar_pose/ARMarker.h>

using namespace std;
using namespace DQ_robotics;
using namespace Eigen;
//
double xp0=0;
double yp0=0;
double zp0=0;
double w0=1;
double rx0=0;
double ry0=0;
double rz0=0;

double xp1=0;
double yp1=0;
double zp1=0;
double w1=1;
double rx1=0;
double ry1=0;
double rz1=0;

double xp2=0;
double yp2=0;
double zp2=0;
double w2=1;
double rx2=0;
double ry2=0;
double rz2=0;

double xp3=0;
double yp3=0;
double zp3=0;
double w3=1;
double rx3=0;
double ry3=0;
double rz3=0;

double xp4=0;
double yp4=0;
double zp4=0;
double w4=1;
double rx4=0;
double ry4=0;
double rz4=0;

double xp5=0;
double yp5=0;
double zp5=0;
double w5=1;
double rx5=0;
double ry5=0;
double rz5=0;

int i0=0;
int i1=0;
int i2=0;
int i3=0;
int i4=0;
int i5=0;

double delta = 0;  // distance in meters from marker 1 to iCreate base
double xodom;
double yodom;
double rzodom;
double rwodom;


 

double readValues0(double d1,double d2,double d3,double d4,double d5, double d6, double d7){
   
   xp0 = d1;
   yp0 = d2;
   zp0 = d3;
   w0  = d4;
   rx0= d5;
   ry0= d6;
   rz0= d7;  	
   i0=1;	   
					   
   return xp0;
   return yp0;
   return zp0;
   return w0;
   return rx0;
   return ry0;
   return rz0;   
   return i0;

}

double readValues1(double d1,double d2,double d3,double d4,double d5, double d6, double d7){
   
   xp1 = d1;
   yp1 = d2;
   zp1 = d3;
   w1  = d4;
   rx1= d5;
   ry1= d6;
   rz1= d7;  				   
   i1=1;	
   return xp1;
   return yp1;
   return zp1;
   return w1;
   return rx1;
   return ry1;
   return rz1;   
   return i1;
}

double readValues2(double d1,double d2,double d3,double d4,double d5, double d6, double d7){
   
   xp2 = d1;
   yp2 = d2;
   zp2 = d3;
   w2  = d4;
   rx2= d5;
   ry2= d6;
   rz2= d7;  				   
   i2=1;
   return xp2;
   return yp2;
   return zp2;
   return w2;
   return rx2;
   return ry2;
   return rz2;  
   return i2; 
}

double readValues3(double d1,double d2,double d3,double d4,double d5, double d6, double d7){
   
   xp3 = d1;
   yp3 = d2;
   zp3 = d3;
   w3  = d4;
   rx3= d5;
   ry3= d6;
   rz3= d7;  				   
   i3=1;
   return xp3;
   return yp3;
   return zp3;
   return w3;
   return rx3;
   return ry3;
   return rz3;
   return i3;    
}

double readValues4(double d1,double d2,double d3,double d4,double d5, double d6, double d7){
   
   xp4 = d1;
   yp4 = d2;
   zp4 = d3;
   w4  = d4;
   rx4= d5;
   ry4= d6;
   rz4= d7; 
   i4=1; 				   

   return xp4;
   return yp4;
   return zp4;
   return w4;
   return rx4;
   return ry4;
   return rz4; 
   return i4;  
}

double readValues5(double d1,double d2,double d3,double d4,double d5, double d6, double d7){
   
   xp5 = d1;
   yp5 = d2;
   zp5 = d3;
   w5  = d4;
   rx5= d5;
   ry5= d6;
   rz5= d7; 
   i5=1; 


   return xp5;
   return yp5;
   return zp5;
   return w5;
   return rx5;
   return ry5;
   return rz5; 
   return i5;  
}



void poseMarkerCallback(const ar_track_alvar_msgs::AlvarMarkers& msg){
	int n;
	n = msg.markers.size(); 
	if ( n == 0) {	
	return;
	cout<<"No markers detected "<<endl;
	}
	else{    
		for ( int i=0; i<n; i++){
			int idframe;
			idframe = msg.markers[i].id;
			if (idframe == 0){
				double x = msg.markers[i].pose.pose.position.x;
				double y = msg.markers[i].pose.pose.position.y;
				double z = msg.markers[i].pose.pose.position.z;
				double w = msg.markers[i].pose.pose.orientation.w;
				double ax =  msg.markers[i].pose.pose.orientation.x;
				double ay =  msg.markers[i].pose.pose.orientation.y;
				double az =  msg.markers[i].pose.pose.orientation.z;   
				readValues0(x,y,z,w,ax,ay,az);
			}
			if (idframe == 1){
				double x = msg.markers[i].pose.pose.position.x;
				double y = msg.markers[i].pose.pose.position.y;
				double z = msg.markers[i].pose.pose.position.z;
				double w = msg.markers[i].pose.pose.orientation.w;
				double ax =  msg.markers[i].pose.pose.orientation.x;
				double ay =  msg.markers[i].pose.pose.orientation.y;
				double az =  msg.markers[i].pose.pose.orientation.z;   
				readValues1(x,y,z,w,ax,ay,az);
			}
		}	         				
	}
}


DQ normalize ( DQ data){
	DQ a = data;
	DQ xn = a*(a.norm().inv());
    return xn;
}


int main(int argc,char** argv){  
	
	double k;	
	DQ rr0(1);
	DQ T0(1);
	DQ r0(1);
	DQ p0(1);
	DQ xk0(1);
	DQ xkant0(1);

	DQ rr1(1);
	DQ T1(1);
	DQ r1(1);
	DQ p1(1);
	DQ xk1(1);
	DQ xkant1(1);

	DQ rr01(1);
	DQ T01(1);
	DQ r01(1);
	DQ p01(1);
	DQ x01(1);
	DQ n01(1);

	int ii=0;   


	ros::init(argc, argv, "Reading_markers");
						//  ros::Rate loop_rate(100);  // to define frequency on ROS
	ros::NodeHandle r;
	ros::NodeHandle nh;
    ros::NodeHandle node;
    ros::NodeHandle bas;
    tf::TransformBroadcaster br;
    tf::Transform transform;

	ros::Subscriber sub_b  = r.subscribe("/ar_pose_marker", 1, &poseMarkerCallback);
	ros::Publisher goal_pub0;
	ros::Publisher goal_pub1;
	ros::Publisher goal_pub6;
	goal_pub0 = nh.advertise<geometry_msgs::PoseStamped>("F_0", 1);
	goal_pub1 = nh.advertise<geometry_msgs::PoseStamped>("F_1", 1);
	goal_pub6 = nh.advertise<geometry_msgs::PoseStamped>("F_01", 1);


	        //--- Variables to measure time and somethings------------------
		        int num_keys1=0, num_keys2=0;
		        struct timeval start, end;
		        long seconds, useconds, timeM;
		        int i=0;
            //--------------------------------------------------------------

           //-----------Defined to save data ------------------------------
		        string save_data = "timeExecuted.txt";
		        char *save_data1 = new char[save_data.length() + 1];
		        strcpy(save_data1, save_data.c_str());
        //---------------------------------------------------------------

	ros::Rate loop_rate(30);  // to define frequency on ROS

	Matrix<double,4,1> position_0;
	position_0 << 0,0,0,0;

	Matrix<double,4,1> orientation_0;
	orientation_0 << 0,0,0,0;

	Matrix<double,4,1> position_1;
	position_1 << 0,0,0,0;

	Matrix<double,4,1> orientation_1;
	orientation_1 << 0,0,0,0;


    Matrix<double,4,1> position_01;
	position_01 << 0,0,0,0;

	Matrix<double,4,1> orientation_01;
	orientation_01 << 0,0,0,0;

	Matrix<double,4,1> n_01;
	n_01 << 0,0,0,0;

	double phi_01=0;
	double mean = 1/1;
	               while(ros::ok()){ 
                    
                        //k=100;
                    //	cout<<"k : "<<k<<endl;   
                    //gettimeofday(&start, NULL);  // start to measure time     
                    //ii++;
                    
			 		//-------------------------------TAG 0------------------------------//			
		     					DQ r0(w0,rx0,ry0,rz0,0,0,0,0);	
								if (w0<0)
								{
								   r0 = -1*r0;
								}
								p0 = i_*xp0 + j_*yp0 + k_*zp0;                    	
								xk0= r0 + E_*0.5*p0*r0;											 																							
								xk0=normalize(xk0);
								xkant0=normalize(xkant0);
								xk0 = xkant0*( ( normalize(xkant0.conj()*xk0) )^(0.005));						
 								
								rr0 = xk0.P();

								orientation_0 = rr0.vec4();
								T0=2*xk0.D()*rr0.conj();
								//T=xk.translation();
								position_0 = T0.vec4();
								
								if (i1==1)
								{
									//cout<<"Robot_base Frame Detected: TAG 1 "<<endl;									
										DQ r1(w1,rx1,ry1,rz1,0,0,0,0);	
										if (w1<0)
										{
										r1 = -1*r1;
										}
										p1 = i_*xp1 + j_*yp1 + k_*zp1;                    	
										xk1= r1 + E_*0.5*p1*r1;											 																							
										xk1=normalize(xk1);
										xkant1=normalize(xkant1);
										xk1 = xkant1*( ( normalize(xkant1.conj()*xk1) )^(mean));
										// 5 mediciones.
										rr1 = xk1.P();
										orientation_1 = rr1.vec4();
										T1=2*xk1.D()*rr1.conj();										
										position_1 = T1.vec4();
								}
						


						//-----compute rigid transformation--x01-----//
                        x01= xk0.conj()*xk1;
                        x01= x01*(1+E_*0.5*(delta*i_));
						rr01 = x01.P();
						orientation_01 = rr01.vec4();
						T01=2*x01.D()*rr01.conj();						
						position_01 = T01.vec4();
						n01= normalize(x01).rot_axis();
						n_01= n01.vec4();
						phi_01=2*acos(orientation_01(0))*180/3.1416;					
                        if (n_01(3) < 0 )
                        {
                        	phi_01=-1*phi_01;
                        }

							//cout<<"--------pose x01-------"<<endl;
							//cout<<"x(m):     "<<position_01(1)<<endl;
							//cout<<"y(m):     "<<position_01(2)<<endl;
							//cout<<"z(m):     "<<position_01(3)<<endl;								    
							//cout<<"phi(deg): "<<phi_01<<endl;	

							//cout<<"axis:     "<<normalize(x01).rot_axis()<<endl;
							//cout<<"                       "<<endl;	
					    //-----------------------------------------------


						
						xkant0= xk0;
						xkant1= xk1;


                    	
						i0=0;
						i1=0;
						
                       //sleep(1);

						transform.setOrigin( tf::Vector3(position_0(1), position_0(2), position_0(3)) );	
						transform.setRotation( tf::Quaternion(orientation_0(1), orientation_0(2), orientation_0(3), orientation_0(0)) );
                        br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "camera_link", "Frame_marker_0"));

                        transform.setOrigin( tf::Vector3(position_01(1), position_01(2), position_01(3)) );	
						transform.setRotation( tf::Quaternion(orientation_01(1), orientation_01(2), orientation_01(3), orientation_01(0)) );
                        br.sendTransform(tf::StampedTransform(transform, ros::Time::now(), "Frame_marker_0", "Frame_marker_1"));

                
						//---------------------------------------------rviz------
					      geometry_msgs::PoseStamped msg;
						  msg.header.frame_id = "camera_link";
						  msg.pose.position.x = position_0(1); 
						  msg.pose.position.y = position_0(2); 
						  msg.pose.position.z = position_0(3);   
						  msg.pose.orientation.w = orientation_0(0); 
						  msg.pose.orientation.x = orientation_0(1); 
						  msg.pose.orientation.y = orientation_0(2);
						  msg.pose.orientation.z = orientation_0(3);  
						  goal_pub0.publish(msg);

						  geometry_msgs::PoseStamped msg1;
						  msg1.header.frame_id = "camera_link";
						  msg1.pose.position.x = position_1(1); 
						  msg1.pose.position.y = position_1(2); 
						  msg1.pose.position.z = position_1(3);   
						  msg1.pose.orientation.w = orientation_1(0); 
						  msg1.pose.orientation.x = orientation_1(1); 
						  msg1.pose.orientation.y = orientation_1(2);
						  msg1.pose.orientation.z = orientation_1(3);  
						  goal_pub1.publish(msg1);

						  geometry_msgs::PoseStamped msg6;
						  msg6.header.frame_id = "camera_link";
						  msg6.pose.position.x = position_01(1); 
						  msg6.pose.position.y = position_01(2); 
						  msg6.pose.position.z = position_01(3);   
						  msg6.pose.orientation.w = orientation_01(0); 
						  msg6.pose.orientation.x = orientation_01(1); 
						  msg6.pose.orientation.y = orientation_01(2);
						  msg6.pose.orientation.z = orientation_01(3);  
						  goal_pub6.publish(msg6);

                        loop_rate.sleep();
 						if (ii==100)     // update frame odom. 5*20ms= 100ms 
 						{
                           
 						}


						//gettimeofday(&end, NULL);
						//seconds  = end.tv_sec  - start.tv_sec;
						//useconds = end.tv_usec - start.tv_usec;
						//timeM = ((seconds) * 1000 + useconds/1000.0) + 0.5;
						//ofstream file_keyteste1(save_data1,ios::app);
						//file_keyteste1 <<timeM<<endl;
						//cout<<"loop time (ms): "<<timeM<<endl;                       
						
						ros::spinOnce();
					}
				

		}



                     
